# Extending LilyPond

**ARCHIVED** (January 2025): This project is unmaintained. Please contact
me if you'd like to pick it up.

This is an unofficial resource on extending LilyPond in Scheme. The rendered
result is found on https://extending-lilypond.gitlab.io. Please read the
"About this document" section on how to build the guide and contribute.
