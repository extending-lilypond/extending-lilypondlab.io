r"""The lily directive shows an image of the result and adds a `\version` line to the code."""

import asyncio
from hashlib import sha256
import importlib.metadata
from os import cpu_count
from os.path import relpath
from pathlib import Path

from docutils import nodes
import lilypond
from sphinx import addnodes
from sphinx.builders.dummy import DummyBuilder
from sphinx.directives import SphinxDirective
from sphinx.errors import SphinxError
from sphinx.locale import _
from sphinx.transforms import SphinxTransform
from sphinx.util.display import status_iterator
from sphinx.util.logging import getLogger
from sphinx_toolbox.collapse import setup as collapse_setup, CollapseNode

logger = getLogger(__name__)

lilypond_version = importlib.metadata.version("lilypond")


def simple_hash(string):
    hasher = sha256()
    hasher.update(string.encode("utf-8"))
    return hasher.hexdigest()


class lily(nodes.compound, addnodes.translatable):
    """A `lily` node contains two subnodes, a literal block for the code and an image for the result."""

    def __init__(self, config, code):
        super().__init__(self)
        self.config = config
        self["untranslated-code-without-version"] = code
        versioned_code = self.update_code(code)
        # Conceptually, the first argument, "rawsource", should be
        # code_without_version, not code_with_version, but Sphinx thinks this
        # means this node is a parsed-literal and no longer highlights it.
        literal = nodes.literal_block(versioned_code, versioned_code)
        self += literal
        # Image added later.

    def add_version(self, code):
        return f'\\version "{lilypond_version}"\n\n{code}'

    def update_code(self, new_code):
        self["code-without-version"] = new_code
        self["code-with-version"] = versioned_code = self.add_version(new_code)
        # Include version in the hash so the snippet is rebuilt if the version changes.
        self["snippet-id"] = snippet_id = simple_hash(versioned_code)
        self["snippet-path"] = Path(self.config.lilypond_images_directory) / snippet_id
        return versioned_code

    def extract_original_messages(self):
        return [self["untranslated-code-without-version"]]

    def preserve_original_messages(self):
        pass

    def apply_translated_message(self, msg, msgstr):
        versioned_code = self.update_code(msgstr)
        literal = self[0]
        new_literal = nodes.literal_block(versioned_code, versioned_code)
        self.replace(literal, new_literal)


class Lily(SphinxDirective):
    required_arguments = 0
    optional_arguments = 0
    final_argument_whitespace = False
    has_content = True

    def run(self):
        code = "\n".join(self.content) + "\n"
        node = lily(self.env.app.config, code)
        self.set_source_info(node)
        self.set_source_info(node[0])
        return [node]


class LilyAddImageNode(SphinxTransform):
    """Sphinx tries to resolve images relative to the source directory.
    We don't want this, since we generate them in a separate directory.
    Therefore, `lily` nodes only get their contained `image` node late
    in the process, after Sphinx's `DoctreeReadEvent` transform, which
    is where images are modified.
    """

    default_priority = 900  # DoctreeReadEvent has 880

    def apply(self, **kwargs):
        if isinstance(self.app.builder, LilyPondImagesBuilder):
            return
        for node in self.document.findall(lily):
            path = node["snippet-path"].with_suffix(".svg")
            # Encapsulate the image in a collapsible, because the code should
            # remain the main focus for the reader, and the output is not
            # always very interesting.
            collapsible = CollapseNode(
                "", _("Output"), classes=["lilypond-output-collapsible"]
            )
            rel_path = relpath(path, (Path(self.app.outdir) / self.env.docname).parent)
            image = nodes.image(uri=str(rel_path), candidates={"*": None})
            collapsible += image
            node += collapsible


class LilyPondImagesBuilder(DummyBuilder):
    name = "lilypond"
    epilog = "Successfully compiled all snippets with LilyPond"

    def get_outdated_docs(self):
        return "LilyPond snippets"  # for logging

    async def process_snippets(self, nodes_to_process):
        while nodes_to_process:
            node = nodes_to_process.pop()
            try:
                path = node["snippet-path"]
                path.parent.mkdir(parents=True, exist_ok=True)
                code = node["code-with-version"]
                path.with_suffix(".ly").write_text(code, encoding="utf-8")
                command = [
                    str(lilypond.executable()),
                    "-dtall-page-formats=svg",
                    "-dno-use-paper-size-for-page",
                    "-dinclude-settings=lilypond-book-preamble.ly",
                    "-dbackend=cairo",
                    "--silent",
                    "--output",
                    str(path),
                    "-",  # read stdin
                ]
                proc = await asyncio.create_subprocess_exec(
                    *command,
                    stdin=asyncio.subprocess.PIPE,
                    stderr=asyncio.subprocess.PIPE,
                    stdout=asyncio.subprocess.DEVNULL,
                )
                stdout, stderr = await proc.communicate(code.encode("utf-8"))

                def error(reason):
                    logger.error(
                        f"LilyPond log:\n\n{stderr.decode('utf-8')}\n\n", location=node
                    )
                    raise SphinxError(reason)

                if proc.returncode != 0:
                    error("LilyPond failed")
                else:
                    if stderr:
                        error("LilyPond warning/error!")
            finally:
                next(self.status)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lily_nodes = []

    def write_doc(self, docname, doctree):
        self.lily_nodes.extend(doctree.findall(lily))

    def finish(self):
        async def run_inner():
            nodes_to_process = []
            for node in self.lily_nodes:
                path = node["snippet-path"]
                if path.with_suffix(".svg").is_file():
                    continue  # already compiled
                nodes_to_process.append(node)
            count = len(nodes_to_process)
            self.status = status_iterator(
                range(count),
                summary="Compiling LilyPond snippets ",
                length=count,
                stringify_func=lambda x: "",
            )
            tasks = [
                asyncio.create_task(self.process_snippets(nodes_to_process))
                for _ in range(cpu_count())
            ]
            await asyncio.gather(*tasks)

        asyncio.run(run_inner())
        logger.info("")


class LilyInnerLiteralNotTranslatable(SphinxTransform):
    """In a lily node, the inner literal node is not translatable, even though
    we have literal_block in gettext_additional_targets, since it's the code
    with a \version statement, so an update of the version would fuzzy lots
    of strings. Instead, the lily node itself is translatable, and its msgid
    does not contain the \version.
    """

    default_priority = 15  # between ExtraTranslatableNodes (10) and Locale (20)

    def apply(self, **kwargs):
        for node in self.document.findall(lily):
            literal = node[0]
            literal["translatable"] = False


def setup(app):
    collapse_setup(app)
    app.add_config_value("lilypond_images_directory", None, "env", [str])
    app.add_node(lily)
    app.add_directive("lily", Lily)
    app.add_builder(LilyPondImagesBuilder)
    app.add_transform(LilyAddImageNode)
    app.add_transform(LilyInnerLiteralNotTranslatable)
