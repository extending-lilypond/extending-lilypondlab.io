"""Directive for describing Scheme functions, in Markdown syntax, with translatable signatures."""

from docutils import nodes
from sphinx import addnodes
from sphinx.directives import ObjectDescription


class untranslated_signature(addnodes.desc_name, addnodes.translatable):
    def preserve_original_messages(self):
        self["original"] = self.astext()

    def apply_translated_message(self, msgid, msgstr):
        self.replace(self.children[0], nodes.Text(msgstr, msgstr))

    def extract_original_messages(self):
        return [self["original"]]


class SchemeFunction(ObjectDescription):
    def get_signatures(self):
        arg = self.arguments[0]
        return [sig.strip() for sig in arg.split("|")]

    def handle_signature(self, sig, signode):
        signode += untranslated_signature(sig, sig)


def setup(app):
    app.add_directive("func", SchemeFunction)
