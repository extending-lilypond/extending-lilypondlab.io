# Awful hack to work around
# https://github.com/executablebooks/MyST-Parser/issues/444

from sphinx.transforms import i18n


class ModifiedIndent:
    def __init__(self, s, _):
        self.s = s

    def __radd__(self, _):
        return f"```\n{self.s}```"


i18n.indent = ModifiedIndent


def setup(app):
    pass
