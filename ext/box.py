from docutils import nodes
from sphinx.directives import SphinxDirective


class Box(SphinxDirective):
    required_arguments = 0
    optional_arguments = 0
    final_argument_whitespace = True
    option_spec = {}
    has_content = True

    def run(self):
        compound_node = nodes.compound()
        compound_node["classes"] = ["box"]
        self.state.nested_parse(self.content, self.content_offset, compound_node)
        return [compound_node]


def setup(app):
    app.add_directive("box", Box)
