"""Markdown syntax for conveniently linking to the official LilyPond documentation.

For example:

```markdown
[link text](internals:Scheme_functions)
```

links to the node "Scheme functions" from the Internals Reference.
Also,

```markdown
[](internals:Scheme_functions)
```

is a link to the same URL, with the link text "Scheme functions".
"""

import importlib.metadata
import re

from docutils import nodes
from sphinx.domains import Domain
from sphinx.util.logging import getLogger

logger = getLogger(__name__)

LILYPOND_MANUAL_LINK_RE = (
    r"(contributor|essay|extending|internals|learning|notation|usage):(.*)"
)


class LilyPondDocumentationDomain(Domain):
    name = "lily-doc"
    label = "LilyPond documentation"

    def resolve_any_xref(self, env, fromdocname, builder, target, node, contnode):
        # We don't have a way to link to a specific micro version on
        # lilypond.org. Once a release is made in an x.y release series, the
        # docs for older versions of that series aren't available anymore.
        (major, minor, _micro) = importlib.metadata.version("lilypond").split(".")
        if match := re.match(LILYPOND_MANUAL_LINK_RE, target):
            manual, target = match.groups()
            if not contnode.astext():
                contnode = nodes.Text(target.replace("_", " "))
            target = target.lower()
            # TODO: what are the escaping rules exactly?
            # We use underscores because using spaces would require putting the target
            # into pointy brackets as soon as it contains a space, i.e.,
            # [link text](<internals:Node name with spaces>), which is cumbersome
            # to remember.
            target = re.sub(
                r"-|_", lambda m: "_002d" if m.group() == "-" else "-", target
            )
            # role_name is only used in warnings for multiple possible resolutions
            # of a reference.
            role_name = "virtual-lilypond-official-doc-role"
            if "#" in target:
                parts = target.split("#")
                try:
                    target, anchor = parts
                    anchor = "#" + anchor
                except ValueError:
                    logger.warning("multiple # characters in doc link", location=node)
                    anchor = ""
            else:
                anchor = ""
            url = f"https://lilypond.org/doc/v{major}.{minor}/Documentation/{manual}/{target}.html{anchor}"
            new_node = nodes.reference("", "", contnode, refuri=url)
            return [(role_name, new_node)]
        return []


def setup(app):
    app.add_domain(LilyPondDocumentationDomain)
