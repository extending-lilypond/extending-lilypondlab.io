```{highlight} scheme-result
```

# Getting started

## Meeting the Scheme sandbox

The Scheme "sandbox" is a convenient way to explore Scheme and do quick tests.
It is an "interactive prompt", or Read-Eval-Print-Loop. Unlike a LilyPond file,
where the contained Scheme code is read and executed in batch, a
Read-Eval-Print-Loop works interactively, by asking for an expression,
evaluating it, and printing the result back. This is much like a pocket
calculator.

To fire up the Scheme sandbox, you first need to open a terminal. On Linux, I
trust you to do that. Under macOS, you need to launch the "Terminal"
application. Under Windows, open whichever folder in the file explorer, and
select "Windows Powershell" in the File menu.

In your terminal, run this command:

```shell
/full/path/to/lilypond-x.y.z/bin/lilypond scheme-sandbox
```

or, on Windows:

```powershell
C:\full\path\to\lilypond-x.y.z\bin\lilypond scheme-sandbox
```

In either case, you should replace the path with the full path to the directory
where you placed LilyPond when you set it up. (If you installed LilyPond through
a package manager or otherwise have LilyPond in your search path, you can just
use `lilypond scheme-sandbox`.)

This command should give you either a screen like this:

```text
GNU LilyPond 2.24.1 (running Guile 2.2)
Processing `.../share/lilypond/2.24.1/ly/scheme-sandbox.ly'
Parsing...
GNU Guile 2.2.7
Copyright (C) 1995-2019 Free Software Foundation, Inc.

Guile comes with ABSOLUTELY NO WARRANTY; for details type `,show w'.
This program is free software, and you are welcome to redistribute it
under certain conditions; type `,show c' for details.

Enter `,help' for help.
scheme@(#{ g100}#)>
```

The line `scheme@...` indicates that Guile is here, ready to listen to your
requests. You can enter a code snippet, and it will evaluate it. Let us start
with the simplest of snippets: a number.

```text
guile> 42
42
```

Remember how I said the sandbox was like a calculator? You typed 42, and since
indeed the value of this number is 42, the interpreter answers back 42. Now let
us write a more interesting computation (the syntax used will be explained
soon):

```text
guile> (+ 42 5)
47
```

Here, we computed 42+5, of which the result is 47.

While you follow this tutorial, you can paste code examples in the sandbox and
play around with variants of these examples. In the rest of this text, by
convention, the symbol "⇒" is used to denote the value of an expression. Suppose
I write:

```
(+ 42 5)
⇒ 47
```

By this, I mean that with the code `(+ 42 5)`, you will get the result 47. In
particular, if you want to test yourself, you can type `(+ 42 5)` in the
sandbox, and it will answer 47. Note that the same convention is used in Guile's
own manual.

To exit the sandbox, enter `(quit)`, or more simply, press Control-D.

To finish this introduction to the Scheme sandbox, I need to mention a detail
that you may have found annoying if you have already started typing some
examples in the sandbox. If you try to delete characters, it may not work and
print strange characters. This can be worked around by installing the
[rlwrap](https://github.com/hanslub42/rlwrap) program, and by using the command
`rlwrap ...lilypond scheme-sandbox` instead of `...lilypond scheme-sandbox`. On
GNU/Linux, `rlwrap` is likely present in your distribution's packages. Under
macOS, you can install it via Homebrew or MacPorts. Under Windows, it is likely
possible to install it through Cygwin, if you feel bold (not for UNIX newbies).

## Inserting Scheme code in a LilyPond file

About anywhere in a LilyPond file, you can switch to Scheme mode by writing a
hash, the "#" character. This reads one complete Scheme expression, and hands
you back to LilyPond when this expression is over. Here are some common
examples:

```lilypond
#(set-global-staff-size 18)
```

```lilypond
\markup \vspace #5
```

```lilypond
\shape #'((0 . 0) (0.1 . 0.3) (0.1 . 0.5) (0 . 1.0)) Slur
```

Scheme is often used to input values, such as the argument 5 to
`\markup \vspace`, or the list of offsets passed to `\shape`. If you type these
values in a Scheme expression that is not passed to LilyPond but just written on
the top-level, it is usually just lost, unlike the sandbox, which prints them.
For example, this minimal LilyPond file produces neither graphical output, nor a
message in the log file:

```lilypond
#'((0 . 0) (0.1 . 0.3) (0.1 . 0.5) (0 . 1.0))
```

It is possible to force a value to be printed by calling the `display` function
on it. This is used like this; the syntax will become clear later.

```lilypond
#(display '((0 . 0) (0.1 . 0.3) (0.1 . 0.5) (0 . 1.0)))
```

Within Scheme code, it is even possible to switch back to LilyPond mode in order
to input some value in LilyPond syntax. This is done by enclosing the LilyPond
code within `#{` and `#}`. For example:

```lilypond
#(skip-of-length #{ { c'8 d' e' f' e' d' } #})
```

## Comments

In LilyPond, comments start with a `%` character, and continue until the end of
the line. Scheme syntax is slightly different; the character used for comments
is not `%`, but `;`. The principle of comments stays the same: they allow to
explain the code for the human reader, or to stash some code away temporarily.
They are annotations in the source file that are completely ignored by the
interpreter.

```
; This is a comment. It has no effect.
42.0 ; Another comment, after a line of code.
```

LilyPond also has multiline comments, which unlike simple `%` comments can span
several lines. They start with `%{` and are terminated by `%}`. Scheme has an
equivalent syntax too; the delimiters are `#!` and `!#`.

```text
#!/usr/bin/guile
Here, we are inside a multiline comment. Users of UNIX
systems (such as GNU/Linux and macOS) may have noticed that
this comment syntax is suitable for shebang lines. If you
don't know what a shebang is, forget about it.
!#
```

## Literals

This part deals with how to input values of frequently used types, those that
typical LilyPond users will meet often.

Let's start with the beginning: numbers. They are written in Scheme just like in
English:

```
5
⇒ 5
-10
⇒ -10
```

Non-integer numbers can be entered using a dot. This sort of number is generally
called a *floating-point number* in computer science.

```
5.3
⇒ 5.3
-10.42
⇒ -10.42
```

Another common type is strings, which represent text as a sequence of
characters. They are input between straight double quotes.

```
"Sonata IV"
⇒ "Sonata IV"
```

They can contain newlines.

```
"Twinkle, twinkle, little star
How I wonder what you are"
⇒ "Twinkle, twinkle, little star
How I wonder what you are"
```

You can also indicate a newline character using `\n`. Both forms are strictly
equivalent.

```
"Twinkle, twinkle, little star\nHow I wonder what you are"
⇒ "Twinkle, twinkle, little star
How I wonder what you are"
```

If the string contains an actual double quote character, you need to "escape" it
using a backslash, so that it is not interpreted as the end of the string.

```
"Rondo \"alla turca\""
⇒ "Rondo \"alla turca\""
```

In order to print a string, you can call the `display` function on it.

```
(display "Rondo \"alla turca\"\n")
⊨ Rondo "alla turca"
```

Now is a good time to introduce another convention for this tutorial. When I
write "⊨", this means that the code prints what follows the "⊨" sign. For
example, the code below prints `Rondo "alla turca"` on the console. Beware of
the difference between printing a string, with `display` (which I denote with
"⊨"), and merely using it without `display` (which I denote using "⇒"). Despite
the superficial similarity, these are vastly different things. When you just do
`"Rondo"`, this gives you the string "Rondo". You can pass this string
somewhere, for example give it to LilyPond:

```lilypond
\header {
  title = "Rondo"
}
```

On the other hand, `display` is just a one-time order to Guile to print the
value on the console. Once Guile has executed this request, it doesn't give you
anything back. `display` does not return a meaningful value. It just prints the
string, which is useful for debugging.

It is important to understand the difference between how a string is input and
how it is displayed. By definition, a string is a sequence of characters.
Despite the superficial look, the string `"\"\"\n"` contains just three
characters, namely two double quotes and one newline. We only need these
backslashes to avoid a confusion when Guile reads the string. They are not part
of the resulting string, as confirmed by `display`:

```
(display "\"\"\n")
⊨ ""
```

It is useful to know that LilyPond recognizes a syntax similar to Scheme's for
numbers and strings. The lines below are equivalent:

```lilypond
\override TextScript.Y-offset = 5.2
\override TextScript.Y-offset = #5.2
```

Similarly, these are equivalent:

```lilypond
title = #"Rondo"
title = "Rondo"
title = Rondo
```

(Of course, the third syntax can only work with a single word. You need the
double quotes as soon as there are spaces in the string.)

(variables)=

## Defining variables

Practically all LilyPond users know that their code will be much easier to
follow and modify if they organize it using variables.

```lilypond
clarinet = \relative {
  ...
}

{ \clarinet }
{ \transpose c d \clarinet }
```

There are several advantages to this. For one thing, once you have defined a
variable, you can reuse it in several places without typing it over again. For
another, even if you do not need to use it in multiple places, using a variable
makes the code more structured and thus easier to understand and later modify.

The syntax to define variables in Scheme is:

```
(define name value)
```

For example, to assign 5 to the variable `x`, you can use:

```
(define x 5)
```

After that, you can refer to the variable simply by its name. This is in
contrast with LilyPond, where referring to a variable is done by writing its
name preceded with a backslash.

```
(define x 5)
(display x)
⊨ 5
```

For multi-word variable names, the convention is to use dashes.

```
(define my-variable 44)
```

Perhaps surprisingly, variable names can contain some special characters.
Notably, these are accepted: `+`, `-`, `*`, `/`, `?`, `!`, `=`, `&`, `|`, `@`. A
few characters are not valid, such as parentheses and quotes. (The precise rules
on valid variable names are somewhat complex, and it is not useful to know about
them in detail.)

```
(define x+|^ 34)
(display x+|^)
⊨ 34
```
