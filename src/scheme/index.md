# Scheme tutorial

This tutorial covers the basics of the Scheme programming language, which is a
prerequisite for writing Scheme extensions to LilyPond.

This material originates in a presentation delivered at a virtual conference of
French-speaking LilyPond users in 2021.

I would like to thank Urs Liska for his own (unfinished)
[book on Scheme and LilyPond](https://scheme-book.readthedocs.io), which helped
me learn Scheme myself, and inspired some parts of this tutorial.

```{toctree}
first-steps
expressions
functions
messages
conditions
local-variables
lists
quoting
alists
recursion
typing
after
```
