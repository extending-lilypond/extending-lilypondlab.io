# Appendix: Other Scheme resources

Here are a few resources to learn more about the Scheme language.

- [LilyPond's Scheme](https://scheme-book.readthedocs.io/), a tutorial that is
  similar to this one. It is unfinished, but some subjects are treated in more
  detail. (It is available in English only, while this one has a French
  version.)

- [Guile's reference manual](https://www.gnu.org/software/guile/docs/docs-2.2/guile-ref/)
  is a vital aid for ambitious projects. Remember to read the manual for the
  right version; this link is for version 2.2, which is the one used in standard
  binaries of LilyPond 2.24.

- Numerous tutorials and books have been written about Scheme, with basic to
  advanced content (and focusing on the language in general, not in the context
  of LilyPond like this tutorial). Two of these books deserve special mention:
  [Structure and Interpretation of Computer Programs](https://web.mit.edu/6.001/6.037/sicp.pdf)
  (by Abelson and Sussman, considered as a Bible by many Schemers), and
  [An Introduction to Scheme and its Implementation](https://www.cs.utexas.edu/ftp/garbage/submit/notready/schintro.ps)
  (by Paul Wilson). Many more can be found on
  [this page](https://schemers.org/Documents/#intro-texts) from
  https://schemers.org. The same website also suggests
  [How to Design Programs](https://htdp.org) (Felleisen, Findler, Flatt and
  Krishnamurti).
