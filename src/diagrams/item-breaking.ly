\version "2.24.0"

%% TODO: auto-compile during build instead of committing the
%% result into Git.

%% TODO: i18n.

#(ly:set-option 'use-paper-size-for-page #f)
#(ly:set-option 'tall-page-formats 'svg)
#(ly:set-option 'backend 'cairo)

\header {
  tagline = ##f
}

\paper {
  page-breaking = #ly:one-page-breaking
  indent = 0
  line-width = 80
}

\layout {
  \context {
    \Score
    measureBarType = ""
    \override SpacingSpanner.spacing-increment = 4
    \remove Bar_number_engraver
  }
  \context {
    \Staff
    \override Clef.break-align-anchor-alignment = -1
    \override Clef.full-size-change = ##t
    \remove Time_signature_engraver
  }
}

\pointAndClickOff

#(define (with-text grob stil text)
   (let* ((text #{ \markup \typewriter \fontsize #-3 #text #})
          (text-stil (grob-interpret-markup grob text))
          (x-trans (- (interval-center (ly:stencil-extent stil X))
                      (interval-center (ly:stencil-extent text-stil X))))
          (text-stil (ly:stencil-translate-axis text-stil x-trans X))
          (text-stil (ly:stencil-translate-axis text-stil 2 Y)))
     (ly:stencil-add stil text-stil)))

#(define scale 0.8)
#(define padding 0.8)

#(define (make-clone-stencil stencil)
   (stencil-with-color
    (ly:stencil-scale stencil scale scale)
    "grey"))

#(define cross-thickness 0.2)
#(define (cross-stencil stencil)
   (ly:stencil-add
    stencil
    (match-let* (((x-start . x-end) (ly:stencil-extent stencil X))
                 ((y-start . y-end) (ly:stencil-extent stencil Y)))
      (stencil-with-color
       (ly:stencil-add
        (make-path-stencil `(moveto ,x-start ,y-start lineto ,x-end ,y-end)
                           cross-thickness 1 1 #f)
        (make-path-stencil `(moveto ,x-start ,y-end lineto ,x-end ,y-start)
                           cross-thickness 1 1 #f))
       "red"))))

{
  \clef treble
  s2
  \once \override Staff.Clef.stencil =
    #(grob-transformer
      'stencil
      (lambda (grob stencil)
        (let* ((clone-stencil (make-clone-stencil stencil))
               (stencil (with-text grob stencil "CENTER"))
               (stencil
                (ly:stencil-combine-at-edge stencil X LEFT (with-text grob clone-stencil "LEFT") padding))
               (stencil
                (ly:stencil-combine-at-edge stencil X RIGHT (with-text grob clone-stencil "RIGHT") padding)))
          stencil)))
  \clef bass
  s2
}

{
  \textMark "Normal case"
  \clef treble
  s2
  \once \override Staff.Clef.stencil =
    #(grob-transformer
      'stencil
      (lambda (grob stencil)
        (let* ((clone-stencil (make-clone-stencil stencil))
               (stencil (with-text grob stencil "CENTER"))
               (stencil
                (ly:stencil-combine-at-edge stencil X LEFT (cross-stencil (with-text grob clone-stencil "LEFT")) padding))
               (stencil
                (ly:stencil-combine-at-edge stencil X RIGHT (cross-stencil (with-text grob clone-stencil "RIGHT")) padding)))
          stencil)))
  \clef bass
  s2
}


{
  \textMark "Broken case"
  \clef treble
  s2
  \once \override Staff.Clef.style = #'zigzag
  \once \override Staff.Clef.zigzag-width = 1.5
  \once \override Staff.Clef.zigzag-length = 0.7
  \once \override Staff.Clef.thickness = 1.5
  \once \override Staff.Clef.stencil =
    #(grob-transformer
      'stencil
      (lambda (grob stencil)
        (let* ((clone-stencil (make-clone-stencil stencil))
               (stencil (cross-stencil (with-text grob stencil "CENTER")))
               (middle (interval-center (ly:stencil-extent stencil X)))
               (stencil
                (ly:stencil-combine-at-edge stencil X LEFT (with-text grob clone-stencil "LEFT") padding))
               (stencil
                (ly:stencil-combine-at-edge stencil X RIGHT (with-text grob clone-stencil "RIGHT") padding))
               (stencil
                (ly:stencil-add stencil
                                (ly:line-interface::line grob middle -5 middle 5)))
               (stencil
                (ly:stencil-combine-at-edge stencil Y UP
                                            (ly:stencil-translate-axis
                                             (centered-stencil (grob-interpret-markup
                                                                grob
                                                                #{ \markup \fontsize #-2 "Line break" #}))
                                             middle X)
                                            0.6))
               (stencil
                (ly:stencil-combine-at-edge stencil Y DOWN
                                            (ly:stencil-translate-axis
                                             (centered-stencil
                                              (grob-interpret-markup
                                               grob
                                               #{
                                                 \markup \scale #'(2 . 2) \overlay {
                                                   \translate #'(-0.2 . 0) \draw-line #'(0 . -1.5)
                                                   \translate #'(+0.2 . 0) \draw-line #'(0 . -1.5)
                                                   \translate #'(0 . -1.5) \general-align #Y #UP \arrow-head #Y #DOWN ##t
                                                 }
                                               #}))
                                             middle X)
                                            1.0)))
          stencil)))
  \clef bass
  s2
}

{
  \clef treble
  s1
  \once \override Staff.Clef.stencil =
    #(grob-transformer
      'stencil
      (lambda (grob original)
        (with-text
         grob
         (make-clone-stencil original)
         (if (eqv? LEFT (ly:item-break-dir grob))
             "LEFT"
             "RIGHT"))))
  \clef bass
  \break
  s1
}
