html_title = "Extending LilyPond"

extensions = [
    # external
    "myst_parser",
    "sphinx_copybutton",
    # internal (in ext/)
    "ext.box",
    "ext.fix_html_title_i18n",
    "ext.fix_myst_code_block_i18n",
    "ext.gen_lang_selecting_index",
    "ext.lang_selector",
    "ext.lily",
    "ext.official_link",
    "ext.scheme_func",
    "ext.typography",
]

myst_enable_extensions = ["deflist", "dollarmath", "linkify"]

html_theme = "pydata_sphinx_theme"

html_static_path = ["../static"]
html_css_files = ["basic-styles.css", "better-copybutton.css", "box.css"]
templates_path = ["../templates"]

html_copy_source = False
html_use_index = False
html_last_updated_fmt = ""
html_theme_options = {
    "icon_links": [
        {
            "name": "GitLab",
            "url": "https://gitlab.com/extending-lilypond/extending-lilypond.gitlab.io",
            "icon": "fa-brands fa-square-gitlab",
            "type": "fontawesome",
        },
    ],
    "pygment_light_style": "lilypond-result",
    "pygment_dark_style": "lilypond-result",
    "footer_start": ["footer/author", "footer/license"],
    "footer_end": ["footer/tool-acknowledgements"],
    # Disable dark theme for now since we don't have a matching
    # Pygments style.
    "navbar_end": ["navbar-icon-links"],
    # Add language selector (always visible, even at small widths,
    # so in navbar_persistent).
    "navbar_persistent": ["search-button", "lang-selector"],
    # Not "Search the docs..."
    "search_bar_text": "Search",
    "use_edit_page_button": True,
}
html_context = {
    # Don't use dark mode even if this is the user's browser/system default.
    # See above.
    "default_mode": "light",
    "gitlab_user": "extending-lilypond",
    "gitlab_repo": "extending-lilypond.gitlab.io",
    "gitlab_version": "main",
    "doc_path": "src",
}

highlight_language = "lilypond"

gettext_additional_targets = ["literal-block"]
gettext_compact = False
gettext_location = False
translation_progress_classes = "untranslated"

suppress_warnings = ["i18n.inconsistent_references"]

lilypond_images_directory = "public/images"
