“Extending LilyPond” — guide to writing LilyPond extensions in Scheme

Written in 2021-2023 by Jean Abou Samra (jean@abou-samra.fr, https://jean.abou-samra.fr)

To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this document to the public domain
worldwide. This document is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication
along with this software, in the file COPYING. If not, see
<http://creativecommons.org/publicdomain/zero/1.0/>.

There is one exception: the file static/icon128px-exported-black.png was
downloaded from https://www.languageicon.org, where it is distributed
under the following terms:

  Relax-Attribution.

    You are suggested but not required to attribute the work when using
    for internet / digital use. You must attribute it in any other use
    which is not on internet. Attribute to: A’ Design Award &
    Competition, Onur Müştak Çobanlı and Farhat Datta with URL
    http://www.languageicon.org for non-internet usage.

  Share Alike.

    If you alter, transform, or build upon this work, you may distribute
    the resulting work only under the same or similar license to this
    one, attribute to original author, resulting work cannot be
    commercial.

  Semi-Noncommercial.

    You may not use this work as a central element (or one of the core
    elements) for commercial purposes; Such as a design on a tshirt, on
    a book cover etc unless you attribute to the author. On the other
    hand you can use freely in your websites without attribution to
    signify language.

  Color-Firendly:

    You can change the colors as you like, keep the form intact please;
    scale proportionally. You can reverse the color or grayscale it if
    required.
